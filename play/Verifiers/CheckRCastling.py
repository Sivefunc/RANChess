def check_r_castling(SCA, LCA, chess_board):
    """
    Return a modified SCA and LCA indicating castling availability
    Short castling and long castling respectively
    
    Using the current chessboard situation and SCA, LCA
    """
    
    if chess_board[0][0] != 'r': LCA = LCA.replace('k','')
        # Upper left corner
    
    if chess_board[0][-1] != 'r': SCA = SCA.replace('k','')
        # Upper right corner

    if chess_board[-1][0] != 'R': LCA = LCA.replace('K','')
        # Lower left corner

    if chess_board[-1][-1] != 'R': LCA = SCA.replace('K','')
        # Lower right corner

    return SCA, LCA
