from .TypeOfMV.Castling.ShortCastling import short_castling
from .TypeOfMV.Castling.LongCastling import long_castling
from .TypeOfMV.Displacement.Displacement_MV import valid_displacement_input
from .TypeOfMV.Displacement.Displacement_MV import displacement_mv
from .TypeOfMV.Capture.Capture_MV import valid_capture_input
from .TypeOfMV.Capture.Capture_MV import capture_mv

def play(
        king_turn, LCA, SCA, message,
        en_passant, chess_board, move):

    if move == 'O-O':
        king_turn, LCA, SCA, message = short_castling(
                king_turn, chess_board,
                LCA, SCA, en_passant)

    elif move == 'O-O-O':
        king_turn, LCA, SCA, message = long_castling(
                king_turn, chess_board,
                LCA, SCA, en_passant)

    elif '-' in move:
        message = valid_displacement_input(move)
        if 'Disabled' in message or 'Activated' in message:
            departure, destination = move.split('-')
            piece, x1, y1 = departure
            if message == "CHDisabled":
                if len(move) == 6:
                    x2, y2 = destination
                    king_turn, SCA, LCA, message = displacement_mv(
                            piece, x1, y1, x2, y2,
                            king_turn, chess_board,
                            en_passant, SCA, LCA)
                
                elif len(move) == 8:
                    x2, y2, prom_sign, prom_piece = destination
                    king_turn, SCA, LCA, message = displacement_mv(
                            piece, x1, y1, x2, y2,
                            king_turn, chess_board,
                            en_passant, SCA, LCA, prom_piece=prom_piece)

            elif message == "CHActivated":
                if len(move) == 7:
                    x2, y2, sign = destination
                    king_turn, SCA, LCA, message = displacement_mv(
                            piece, x1, y1, x2, y2,
                            king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True)

                elif len(move) == 9:
                    x2, y2, prom_sign, prom_piece, sign = destination
                    king_turn, SCA, LCA, message = displacement_mv(
                            piece, x1, y1, x2, y2,
                            king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, prom_piece=prom_piece)

            elif message == "CHMActivated":
                if len(move) == 7:
                    x2, y2, sign = destination
                    king_turn, SCA, LCA, message = displacement_mv(
                            piece, x1, y1, x2, y2,
                            king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, checkmate=True)
                
                elif len(move) == 9:
                    x2, y2, prom_sign, prom_piece, sign = destination
                    king_turn, SCA, LCA, message = displacement_mv(
                            piece, x1, y1, x2, y2,
                            king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, checkmate=True,
                            prom_piece=prom_piece)

    elif 'x' in move:
        message = valid_capture_input(move)
        if 'Disabled' in message or 'Activated' in message:
            departure, destination = move.split('x')
            piece1, x1, y1 = departure
             
            if message == "CHDisabled":
                if len(move) == 7:
                    piece2, x2, y2 = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA)
                
                elif len(move) == 9:
                    piece2, x2, y2, prom_sign, prom_piece = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA, prom_piece=prom_piece)
                
                elif len(move) == 11:
                    piece2, x2, y2, space, e, dot, p = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA, ep_mv=True)

            elif message == "CHActivated":
                if len(move) == 8:
                    piece2, x2, y2, sign = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True)

                elif len(move) == 10:
                    (piece2, x2, y2,
                            prom_sign, prom_piece, sign) = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, prom_piece=prom_piece)

                elif len(move) == 12:
                    piece2, x2, y2, space, e, dot, p, sign = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA, check=True, ep_mv=True)

            elif message == "CHMActivated":
                if len(move) == 8:
                    piece2, x2, y2, sign = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, checkmate=True)
                
                elif len(move) == 10:
                    (piece2, x2, y2,
                            prom_sign, prom_piece, sign) = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, checkmate=True,
                            prom_piece=prom_piece)
                
                elif len(move) == 12:
                    piece2, x2, y2, space, e, dot, p, sign = destination
                    king_turn, SCA, LCA, message = capture_mv(
                            piece1, x1, y1, x2, y2,
                            piece2, king_turn, chess_board,
                            en_passant, SCA, LCA,
                            check=True, checkmate=True,ep_mv=True)
    else:
        message = "Unknown command"
    return king_turn, SCA, LCA, message
