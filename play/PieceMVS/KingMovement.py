def king_movements(X1, Y1, chess_board):
    """
    Return a list with all possible legal moves of the king
    using his given position (X1, Y1) in a given chess_board
    """
    
    attacking_piece = chess_board[Y1][X1]

    moves = [
        [X1 - 1, Y1 - 1],
        [X1 + 1, Y1 - 1],
        [X1, Y1 - 1],
        [X1 - 1, Y1],
        [X1 + 1, Y1],
        [X1 - 1, Y1 + 1],
        [X1 + 1, Y1 + 1],
        [X1, Y1 + 1]
    ] # Possible king moves (Incluiding legal and illegal)

    for move in moves.copy():
        X2, Y2 = move

        if (X2 < 0 or X2 >= len(chess_board) or 
            Y2 < 0 or Y2 >= len(chess_board)):
                # Move is not on the board
            moves.remove(move)
        
        else: # Move is on the board
            if chess_board[Y2][X2] != '0':
                target_piece = chess_board[Y2][X2]
                
                if (target_piece.islower() and attacking_piece.islower() or
                    target_piece.isupper() and attacking_piece.isupper()):
                        # Piece from the player itself
                    moves.remove(move) 
    return moves
