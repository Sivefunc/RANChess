import copy
from play.models import rows, columns, pieces
from play.TypeOfMV.LegitDeparture.LegitDeparture import legit_departure
from play.TypeOfMV.Displacement.messages import *
from play.TypeOfMV.LegitNotation.LegitNotation import legit_notation_mv
from play.TypeOfMV.MakeMovement.Make_Movement import make_movement
from play.Verifiers.CheckMate import mv_that_puts_their_own_k_in_ck
from play.PieceMVS.ChooseMovement import choose_movements

def valid_displacement_input(move):
    """
    Return a bool if it's a legit displacement input if not return a message
    using the given move input
    """

    if 'x' not in move: 
        # It can't contain X, Because it's displacement not capture
        if move.count('-') == 1: # It needs to have only 1 '-' neither < nor >
            if move.find('-') == 3: # If his location eq 3 [000-00]
                if len(move) == 6: # mv -> [000-00]
                    if ('#' not in move and '+' not in move and
                        '=' not in move and ' ' not in move and
                        '.' not in move):

                        return DMVCheckDisabled
                    return DNormalMVContainIllegalSign

                elif len(move) == 7: # mv -> [000-000]
                    if '+' in move:
                        if ('#' not in move and '=' not in move and
                            ' ' not in move and '.' not in move):

                            if move.endswith('+'):
                                if move.count('+') == 1:
                                    return DMVCheckActivated
                                return DNormalchMVContainMoreThan1CH
                            return DNormalchMVSignWrongPlace
                        return DNormalchMVContainIllegalSign

                    elif '#' in move:
                        if ('+' not in move and '=' not in move and
                            ' ' not in move and '.' not in move):
                            
                            if move.endswith('#'):
                                if move.count('#') == 1:
                                    return DMVCheckmateActivated
                                return DNormalchmMVContainMoreThan1CHM
                            return DNormalchmMVSignWrongPlace
                        return DNormalchmMVContainIllegalSign

                    else:
                        return DMVeq7chDoesntContainCHorCHM
                
                elif len(move) == 8: # move -> [000-0000]
                    if '=' in move:
                        if move.index('=') == 6:
                            if move.count('=') == 1:
                                if ('#' not in move and '+' not in move and
                                    ' ' not in move and '.' not in move):
                                    return DMVCheckDisabled
                                return DPromMVContainIllegalSign
                            return DPromMVContainMoreThan1EQ
                        return DPromMVSignWrongPlace
                    return DMVeq8chDoesntContainEQ

                elif len(move) == 9:
                    if '=' in move:
                        if move.index('=') == 6:
                            if move.count('=') == 1:
                                if '+' in move:
                                    if ('#' not in move and ' ' not in move and
                                        '.' not in move):
                                        if move.endswith('+'):
                                            if move.count('+') == 1:
                                                return DMVCheckActivated
                                            return DPromchMVContainMoreThan1CH
                                        return DPromchMVSignWrongPlace
                                    return DPromchMVContainIllegalSign

                                elif '#' in move:
                                    if ('+' not in move and ' ' not in move and
                                        '.' not in move):
                                        if move.endswith('#'):
                                            if move.count('#') == 1:
                                                return DMVCheckmateActivated
                                            return DPromchmMVContainMoreThan1CHM
                                        return DPromchmMVSignWrongPlace 
                                    return DPromchmMVContainIllegalSign
                                
                                else:
                                    return DPromMVDoesntContainCHorCHM

                            return DPromMVContainMoreThan1EQ
                        return DPromMVSignWrongPlace
                    return DMVeq9chDoesntContainEQ

                else:
                    return DMVHasToBeBetween6and9ch

            else:
                return DMVHyphenNotRightPlace 

        else:
            return DMVContainMoreThan1Hyphen
    
    else:
        return DMVContainXandHyphen

def displacement_mv(
        piece, X1, Y1, X2, Y2,
        king_turn, chess_board,
        en_passant=None, SCA=None, LCA=None,
        check=None, checkmate=None, prom_piece=None):

    """
    Return a tuple indicating king_turn, SCA, LCA and a message
    using a piece, his position and destination in a chessboard and king turn
    
    LCA = Long castling availability also Queenside castling
    SCA = Short castling availability also Kingside castling

    optional: en_passant, SCA and LCA
    """
    if piece in pieces:
            # Algebraic notation Departure piece has to be upper case
            # It doesn't matter if the piece is black or white
        result = legit_departure(piece, X1, Y1, king_turn, chess_board)
        
        if result == True: # If it's a legit departure
            X1, Y1 = columns.get(X1), rows.get(Y1) # Departure
                # Transform number and letter to corresponding list index

            if Y2 in rows.keys() and X2 in columns.keys():
                    # Y2 is a str(number) and belongs to the range 1-8
                    # X2 is a str(letter) and belongs to the range a-h
                X2, Y2 = columns.get(X2), rows.get(Y2) # Destination
                    # Transform number and letter to corresponding list index

                if chess_board[Y2][X2] == '0':
                        # Destination square empty

                    X3, Y3 = en_passant[1]
                    if ([X3 - 1, Y3] == [X1, Y1] and piece == 'P' or
                        [X3 + 1, Y3] == [X1, Y1] and piece == 'P'):
                            # The pawn is in the left or right of e.p Piece

                        if ([X3, Y3 - 1] == [X2, Y2] and king_turn.isupper() or
                            [X3, Y3 + 1] == [X2, Y2] and king_turn.islower()):
                                # Pawn is going to take e.p privilege
                            return king_turn, SCA, LCA, DMVepPriv

                    testing_en_passant = en_passant.copy()
                    piece_moves = choose_movements(
                            X1, Y1, chess_board,
                            testing_en_passant)
                    if [X2, Y2] in piece_moves:
                            # If destination is in the mvs of given piece

                        if not mv_that_puts_their_own_k_in_ck(
                                king_turn, [X2, Y2], chess_board,
                                X1, Y1, en_passant):
                                # If mv doesn't put their own king in check
                            
                            message = legit_notation_mv(
                                    X1, Y1, X2, Y2, king_turn,
                                    en_passant, chess_board, piece,
                                    check, checkmate, prom_piece)
                            
                            if message is True:
                                # then make move
                                king_turn, SCA, LCA, message = make_movement(
                                            X1, Y1, X2, Y2,
                                            en_passant,
                                            chess_board, piece,
                                            king_turn, SCA, LCA, prom_piece)
                        
                        else: # Mv puts their own king in check
                            message = DMVItWillCauseCheck

                    else: # Destination is not in the possible mvs of the Piece
                        message = DMVPieceCantMVToSQ

                else: # Destination square is not empty
                    message = DMVPieceInTheDionSQ

            else: # Destination square doesn't exist
                message = DMVDionSQDoesntExist

        else: # It's not a legit departure
            message = result
    
    else: # Departure piece is not in existing pieces
        if piece.isupper():
            message = DMVDeparturePieceDoesntExist
        
        else:
            message = DMVDreHasToBeUpper

    return king_turn, SCA, LCA, message  
