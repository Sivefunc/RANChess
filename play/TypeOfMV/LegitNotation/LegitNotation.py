import copy
from play.TypeOfMV.LegitNotation.messages import *
from play.Verifiers.CheckMate import king_in_check, king_in_checkmate
from play.Verifiers.CheckMate import mv_that_puts_their_own_k_in_ck,check_en_passant

def legit_notation_mv(
        X1, Y1, X2, Y2, king,
        testing_en_passant, chess_board, piece,
        check=None, checkmate=None, prom_piece=None
        ):
    
    testing_board = copy.deepcopy(chess_board)
    
    testing_king = king[:]
    testing_ep = check_en_passant(
            X1, Y1, X2, Y2,
            testing_en_passant.copy(), testing_board)
            # Modifying en_passant privilege
    
    # Making the movement
    testing_board[Y2][X2] = testing_board[Y1][X1]
    testing_board[Y1][X1] = '0'
    
    if piece == 'P': 
        # A pawn moved, and apparently it's a promotion move
        if Y2 == 0:
            if prom_piece is not None:
                # If pawn destination is first row: then it's white piece
                if prom_piece in [
                    'Q','R',
                    'N','B']: # Pawn will be promoted to 1 of these piece

                    testing_board[Y2][X2] = prom_piece
                        # Change destination square to that promotion piece
                
                else: # Input is not in list
                    if prom_piece.islower():
                        return PromotionPieceHasToBeUpper
                    return PromotionPieceNotAvailable
            else:
                return NormalMVButItsPromotionMV

        elif Y2 == len(chess_board) - 1:
            if prom_piece is not None:
                # If pawn destination is last row: then it's black piece
                if prom_piece in [
                    'Q','R',
                    'N','B']: # Pawn will be promoted to 1 of these piece

                    testing_board[Y2][X2] = prom_piece.lower()
            
                else: # Input is not in list
                    if prom_piece.islower():
                        return PromotionPieceHasToBeUpper
                    return PromotionPieceNotAvailable
            else:
                return NormalMVButItsPromotionMV
        else:
            if prom_piece is not None:
                return NormalMVContainPromNotation
    else:
        if prom_piece is not None:
            return NormalMVContainPromNotation

    if testing_king == 'k': testing_king = 'K'
    elif testing_king == 'K': testing_king = 'k'
    
    # Checking if it's a check mv mv or normal mv
    if check is not None:
        # Checking if mv puts enemy king on check
        if king_in_check(
                testing_king, testing_board,
                testing_ep):
             
            # Checking if it's a checkmate mv or check mv
            if checkmate is not None:
                
                # Checking if mv puts enemy king on checkmate
                if king_in_checkmate(
                        testing_king, testing_board,
                        testing_ep):
                    
                    return True

                else: # It doesn't, Illegal notation 
                    return MoveIsCHMButDoesntPutKInCHM

            else: # It's check move
                
                # Checking if check mv puts enemy king on checkmate
                if king_in_checkmate(
                    testing_king, testing_board,
                    testing_ep):
                    # It does, Illegal notation

                    return MoveIsCHButPutKInCHM

                else: # King in check not checkmate
                        # Legal notation
                    
                    return True

        else: # It doesn't, illegal notation
            if checkmate is not None:
                return MoveIsCHMButDoesntPutKInCHM
            
            else:
                return MoveIsCHButDoesntPutKInCH

    else: # It's normal move

        # Checking if normal mv puts enemy king on check
        if king_in_check(
                testing_king, testing_board,
                testing_ep):
            
            # It does, Illegal notation
            return MoveIsNormalButPutKingInCheck
        
        else: # It doesn't, Legal notation
            return True
