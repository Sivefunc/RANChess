import copy
from play.models import rows, columns, pieces
from play.TypeOfMV.LegitDeparture.LegitDeparture import legit_departure
from play.TypeOfMV.Capture.messages import *
from play.TypeOfMV.LegitNotation.LegitNotation import legit_notation_mv
from play.TypeOfMV.MakeMovement.Make_Movement import make_movement
from play.Verifiers.CheckMate import mv_that_puts_their_own_k_in_ck
from play.PieceMVS.ChooseMovement import choose_movements

def valid_capture_input(move):
    """
    Return a bool if it's a legit displacement input if not return a message
    using the given move input
    """

    if '-' not in move:
        # It can't contain '-', because it's a capture not displacement
        if move.count('x') == 1: # It needs to have only 1 'x' neither < nor >
            if move.find('x') == 3: # If his location eq 3 [000x000]
                if len(move) == 7: # mv -> [000x000]
                    if ('#' not in move and '+' not in move and
                        '=' not in move and ' ' not in move and
                        '.' not in move):

                        return CMVCheckDisabled
                    return CNormalMVContainIllegalSign
                
                elif len(move) == 8: # mv -> [000x0000]
                    if '+' in move:
                        if ('#' not in move and '=' not in move and
                            ' ' not in move and '.' not in move):
                            
                            if move.endswith('+'):
                                if move.count('+') == 1:
                                    return CMVCheckActivated
                                return CNormalchMVContainMoreThan1CH
                            return CNormalchMVSignWrongPlace
                        return CNormalchMVContainIllegalSign

                    elif '#' in move:
                        if ('+' not in move and '=' not in move and
                            ' ' not in move and '.' not in move):
                            if move.endswith('#'):
                                if move.count('#') == 1:
                                    return CMVCheckmateActivated
                                return CNormalchmMVContainMoreThan1CHM
                            return CNormalchmMVSignWrongPlace 
                        return CNormalchmMVContainIllegalSign
                    
                    else:
                        return CMVeq8chDoesntContainCHorCHM
                
                elif len(move) == 9: # move -> [000x00000]
                    if '=' in move:
                        if move.index('=') == 7:
                            if move.count('=') == 1:
                                if ('#' not in move and '+' not in move and
                                    ' ' not in move and '.' not in move):
                                    return CMVCheckDisabled
                                return CPromMVContainIllegalSign
                            return CPromMVContainMoreThan1EQ
                        return CPromMVSignWrongPlace
                    return CMVeq9chDoesntContainEQ
                    
                elif len(move) == 10: # move -> [000x000000]
                    if '=' in move:
                        if move.index('=') == 7:
                            if move.count('=') == 1:
                                if '+' in move:
                                    if ('#' not in move and ' ' not in move and
                                        '.' not in move):
                                        if move.endswith('+'):
                                            if move.count('+') == 1:
                                                return CMVCheckActivated
                                            return CPromchMVContainMoreThan1CH
                                        return CPromchMVSignWrongPlace
                                    return CPromchMVContainIllegalSign

                                elif '#' in move:
                                    if ('+' not in move and ' ' not in move and
                                        '.' not in move):
                                        if move.endswith('#'):
                                            if move.count('#') == 1:
                                                return CMVCheckmateActivated
                                            return CPromchmMVContainMoreThan1CHM
                                        return CPromchmMVSignWrongPlace
                                    return CPromchmMVContainIllegalSign
                                
                                else:
                                    return CPromMVDoesntContainCHorCHM

                            return CPromMVContainMoreThan1EQ
                        return CPromMVSignWrongPlace
                    return CMVeq10chDoesntContainEQ

                elif len(move) == 11: # move -> [000x0000000]
                    if ' e.p' in move:
                        if move.index(' e.p') == 7:
                            if move.count('e.p') == 1:
                                if ('#' not in move and '+' not in move
                                        and '=' not in move):
                                    return CMVCheckDisabled
                                return CEPMVContainIllegalSign
                            return CEPMVContainMoreThan1EP
                        return CEPMVSignWrongPlace
                    return CMVeq11chDoesntContainEP

                elif len(move) == 12: # move -> [000x00000000]
                    if ' e.p' in move:
                        if move.index(' e.p') == 7:
                            if move.count('e.p') == 1:
                                if '+' in move:
                                    if '#' not in move and '=' not in move:
                                        if move.endswith('+'):
                                            if move.count('+') == 1:
                                                return CMVCheckActivated
                                            return CEPchMVContainMoreThan1CH
                                        return CEPchMVSignWrongPlace
                                    return CEPchMVContainIllegalSign

                                elif '#' in move:
                                    if '+' not in move and '=' not in move:
                                        if move.endswith('#'):
                                            if move.count('#') == 1:
                                                return CMVCheckmateActivated
                                            return CEPchMVContainMoreThan1CHM
                                        return CEPchmMVSignWrongPlace
                                    return CEPchmMVContainIllegalSign
                                
                                else:
                                    return CEPMVDoesntContainCHorCHM
                            return CEPMVContainMoreThan1EP
                        return CEPMVSignWrongPlace
                    return CMVeq12chDoesntContainEP

                else:
                    return CMVHasToBeBetween7and12ch

            else:
                return CMVXNotRightPlace
        
        else:
            return CMVContainMoreThan1X

    else:
        return CMVContainXandHyphen
    
def capture_mv(
        piece1, X1, Y1, X2, Y2,
        piece2, king_turn, chess_board,
        en_passant=None, SCA=None, LCA=None,
        check=None, checkmate=None, prom_piece=None,ep_mv=None):

    """
    Return a tuple indicating king_turn, SCA, LCA and a message
    using a piece, his position and destination in a chessboard and king turn

    
    LCA = Long castling availability also Queenside castling
    SCA = Short castling availability also Kingside castling

    optional: en_passant, SCA and LCA
    """

    if piece1 in pieces and piece2 in pieces:
            # Algebraic notation Departure piece and Destination piece (Upper)
            # It doesn't matter if the pieces are black or white

        if piece1.upper() and piece2.upper():
            result = legit_departure(piece1, X1, Y1, king_turn, chess_board) 

            if result == True:  # If it's a legit departure
                X1, Y1 = columns.get(X1), rows.get(Y1)
                    # Transform number and letter to corresponding list index

                if Y2 in rows.keys() and X2 in columns.keys():
                        # Y2 is a str(number) and belongs to the range 1-8
                        # X2 is a str(letter) and belongs to the range a-h
                    X2, Y2 = columns.get(X2), rows.get(Y2)
                        # Transform number and letter to corresponding list index
                    
                    X3, Y3 = en_passant[1]
                    if (chess_board[Y2][X2].upper() == piece2 or
                        en_passant[0] == True and piece1 == 'P'):
                            # Destination piece is in destination square or
                            # It's taking a possible e.p privilege
                        
                        if chess_board[Y2][X2] == '0':
                                # Dest. square is empty, it's taking
                                # a possible e.p privilege

                            if ([X3 - 1, Y3] != [X1, Y1] and
                                [X3 + 1, Y3] != [X1, Y1]):
                                    # The pawn is not either in the left nor
                                    # right of e.p Piece
                                if ([X3, Y3 - 1] != [X2, Y2] and
                                    [X3, Y3 + 1] != [X2, Y2]):
                                        # It's not taking e.p privilege

                                    if ep_mv is not None:
                                        message = EnPassantMVButItsCNormalMV
                                        return king_turn, SCA, LCA, message
                                    message = PawnCantCaptureEmptySQ
                                    return king_turn, SCA, LCA, message

                                else:
                                    if ep_mv is not None:
                                        message = EnPassantMVButItsCNormalMV
                                        return king_turn, SCA, LCA, message
                                    message = PawnCantCaptureEmptySQ
                                    return king_turn, SCA, LCA, message

                            else: # Pawn is in the left or right of e.p piece
                                if ([X3, Y3 - 1] != [X2, Y2] and
                                    [X3, Y3 + 1] != [X2, Y2]): 
                                        # It's not taking e.p privilege

                                    if ep_mv is not None:
                                        message = EnPassantMVButItsCNormalMV
                                        return king_turn, SCA, LCA, message
                                    message = PawnCantCaptureEmptySQ
                                    return king_turn, SCA, LCA, message

                        else:
                            if ep_mv == True:
                                message = EnPassantMVButItsCNormalMV
                                return king_turn, SCA, LCA, message

                        testing_en_passant = en_passant.copy()
                        piece_moves = choose_movements(
                                X1, Y1, chess_board,
                                testing_en_passant)
                        
                        if [X2, Y2] in piece_moves:
                                # If destination is in the mvs of given piece

                            if not mv_that_puts_their_own_k_in_ck(
                                    king_turn, [X2, Y2], chess_board,
                                    X1, Y1, en_passant):
                                    # If mv doesn't put their own king in check

                               message = legit_notation_mv(
                                    X1, Y1, X2, Y2, king_turn,
                                    en_passant, chess_board, piece1,
                                    check, checkmate, prom_piece)

                               if message is True:
                                    king_turn, SCA, LCA, message = make_movement(
                                            X1, Y1, X2, Y2,
                                            en_passant,
                                            chess_board, piece1,
                                            king_turn, SCA, LCA, prom_piece)
                            
                            else: # Move puts their own king in check
                                message = CMVItWillCauseCheck
                        
                        else: # Destination is not in the possible mvs of the Piece
                            message = CMVPieceCantMVToSQ

                    else: # Destination piece is not in destination square
                          # Or e.p priv. not available and piece not pawn
                        if ep_mv == True:
                            if en_passant[0] == True:
                                if chess_board[Y2][X2] == '0':
                                    message = EnPassantMVButPieceIsNotPawn

                                else:
                                    message = EnPassantMVButPieceInDestSquare
                            else:
                                message = EnPassantMVButEPprivNotAvailable
                        else:
                            message = CMVPieceIsntIntheDionSQ

                else: # Destination square doesn't exist
                    message = CMVDionSQDoesntExist
            
            else: # It's not a legit departure
                message = result
        
        else:
            message = CMVPiecesDontExist

    else: # Dre. and Dest. pieces are not in existing pieces
        if piece1.isupper() and piece2.isupper():
            message = CMVDreAndDestPiecesDontExist
        
        else:
            message = CMVDreAndDionHaveToBeUpper

    return king_turn, SCA, LCA, message
