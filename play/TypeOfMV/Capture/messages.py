# ------------ Wrong Input --------------------
CMVDreAndDionHaveToBeUpper = "Departure and Destination pieces have to be Upper Case letters"
CMVDionSQDoesntExist = "Destination square doesn't exist in the board"
CMVPieceIsntIntheDionSQ = "Piece is not located in the destination square"
CMVPieceCantMVToSQ = "Your piece can't move to that square"
CMVItWillCauseCheck = "You can't capture the piece, It will cause check in your own king"
CMVPieceBelongsToYou = "You can't capture a piece that belongs to youi"
EnPassantMVButItsCNormalMV = "Your move is en passant but it is a capture normal mv"
EnPassantMVButEPprivNotAvailable = "Your move is en passant but en passant priv is not available"
EnPassantMVButPieceIsNotPawn = "Your move is en passant but your piece is not a pawn"
PawnCantCaptureEmptySQ = "Your pawn can't capture an empty square"
EnPassantMVButPieceInDestSquare = "Your mv is en passant but destination square not empty"
CMVDreAndDestPiecesDontExist = "Departure and destination pieces don't exist"
# ----------------------------------------------------- #
# Valid capture inpu func messages

CMVXNotRightPlace = "The 'x' is not in the right place"
CMVContainMoreThan1X = "Your move contains more than 1 'x'"
CMVContainXandHyphen = "Your move can't be Capture and Displacement at the same time"
CMVHasToBeBetween7and12ch = "Your move has to be between 7 and 12 characters"

CMVCheckDisabled = "CHDisabled"
CMVCheckActivated = "CHActivated"
CMVCheckmateActivated = "CHMActivated"

# ---------- P R O M O T I O N -----------#
CPromMVSignWrongPlace = "Your promotion mv doesn't contains the '=' sign in the right place"
CPromMVContainMoreThan1EQ = "Your promotion mv contains more than 1 '=' sign"

# ---------- E N . P A S S A N T ---------#
CEPMVSignWrongPlace = "Your en passant mv doesn't contain the 'e.p' sign in the right place"
CEPMVContainMoreThan1EP = "Your en passant mv contains more than 1 'e.p' sign"

# ---------- MV Equal 7 ch long ---------- #
CNormalMVContainIllegalSign = "Your normal mv 7 ch long contain illegal sign"

# ---------- MV Equal 8 ch long --------- #
CNormalchMVContainIllegalSign = "Your normal check mv 8 ch long contain illegal sign"
CNormalchMVSignWrongPlace = "Your normal check mv 8 ch long doesn't ends with '+' sign"
CNormalchMVContainMoreThan1CH = "Your normal check mv contain more than 1 '+' sign"

CNormalchmMVContainIllegalSign = "Your normal checkmate mv 8 ch long contain illegal sign"
CNormalchmMVSignWrongPlace = "Your normal checkmate mv 8 ch long doesn't ends with '#' sign"
CNormalchmMVContainMoreThan1CHM = "Your normal checkmate mv contain more than 1 '#' sign"

CMVeq8chDoesntContainCHorCHM = "Your mv is eq 8 ch long, but it doesn't contain '#' or '+' sign"

# ---------- MV Equal 9 ch long --------- #
CMVeq9chDoesntContainEQ = "Your mv is eq 9 ch long, but it doesn't contain '=' sign"
CPromMVContainIllegalSign = "Your promotion mv contains illegal sign"

# ---------- MV Equal 10 ch long --------- #
CMVeq10chDoesntContainEQ = "Your mv is eq 10 ch long, but it doesn't contain '=' sign"
CPromMVDoesntContainCHorCHM = "Your promotion mv is eq 10 ch long, but it doesn't contain '#' or '+' sign"
CPromchMVContainIllegalSign = "Your promotion check mv 10 ch long contain illegal sign"
CPromchMVSignWrongPlace = "Your promotion check mv 10 ch long doesn't ends with '+' sign"
CPromchMVContainMoreThan1CH = "Your promotion check mv contain more than 1 '+' sign"

CPromchmMVContainIllegalSign = "Your promotion checkmate mv 10 ch long contain illegal sign"
CPromchmMVSignWrongPlace = "Your promotion checkmate mv 10 ch long doesn't ends with '#' sign"
CPromchmMVContainMoreThan1CHM = "Your promotion checkmate mv contain more than 1 '#' sign"

# ---------- MV Equal 11 ch long --------- #
CMVeq11chDoesntContainEP = "Your mv is eq 11 ch long, but it doesn't contain 'e.p' sign"
CEPMVContainIllegalSign = "Your en passant mv contains illegal sign"

# ---------- MV Equal 12 ch long --------- #
CMVeq12chDoesntContainEP = "Your mv is eq 12 ch long, but it doesn't contain 'e.p' sign"
CEPMVDoesntContainCHorCHM = "Your en passant mv is eq 12 ch long, but it doesn't contain '#' or '+' sign"
CEPchMVContainIllegalSign = "Your en passant check mv 12 ch long contain illegal sign"
CEPchMVSignWrongPlace = "Your en passant check mv 12 ch long doesn't ends with '+' sign"
CEPchMVContainMoreThan1CH = "Your en passant check mv contain more than 1 '+' sign"

CEPchmMVContainIllegalSign = "Your en passant checkmate mv 12 ch long contain illegal sign"
CEPchmMVSignWrongPlace = "Your en passant checkmate mv 12 ch long doesn't ends with '#' sign"
CEPchmMVContainMoreThan1CHM = "Your en passant checkmate mv contain more than 1 '#' sign"
