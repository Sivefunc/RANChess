import os
import copy
import random
from play.play import play
from play.TypeOfMV.MakeMovement.messages import white_king_in_check
from play.TypeOfMV.MakeMovement.messages import black_king_in_check
from Graphics.GenGraphics import *
from Graphics.CenterText import center_text
from Graphics.WrongMove import wrong_move
from Graphics.PrintGMNotation import print_game_notation

def main():
    if os.get_terminal_size()[0] < 45 or os.get_terminal_size()[1] < 30:
        os.system("clear")
        print("Terminal smaller than 45x30")
        exit()

    # variables that the game needs
    king_turn = "K"
    chess_board = [
        ['r','n','b','q','k','b','n','r'],
        ['p','p','p','p','p','p','p','p'],
        ['0','0','0','0','0','0','0','0'],
        ['0','0','0','0','0','0','0','0'],
        ['0','0','0','0','0','0','0','0'],
        ['0','0','0','0','0','0','0','0'],
        ['P','P','P','P','P','P','P','P'],
        ['R','N','B','Q','K','B','N','R']
        ]

    LCA = "Kk" # Long castling availability
    SCA = "Kk" # Short castling availability
    message = ''
    en_passant = [False, [-1, -1]]

    # Optional var to make it interactive
    game_notation = {}
    plays = 1

    # Start
    os.system("clear")
    print(center_text(os.get_terminal_size()[0], "RANChess"))
    print("")
    print_new_graphics(gen_graphics(chess_board))
    print(message)
   
    while king_turn == 'K' or king_turn == 'k':
        if os.get_terminal_size()[0] < 45 or os.get_terminal_size()[1] < 30:
            os.system("clear")
            print("Terminal smaller than 45x30")
            exit()
             
        ch_limit = os.get_terminal_size()[0]
        if king_turn == 'K':
            move = input(center_text(ch_limit,
                            "White player move ->").rstrip() + ' ')
            # Reversible Algebraic notation
        
        else:
            move = input(center_text(ch_limit,
                            "Black player move ->").rstrip() + ' ') 
            # Reversible Algebraic notation
        
        if os.get_terminal_size()[0] < 45 or os.get_terminal_size()[1] < 30:
            os.system("clear")
            print("Terminal smaller than 45x30")
            exit()

        king_turn, SCA, LCA, message = play(
                king_turn, LCA, SCA, message,
                en_passant, chess_board, move)

        ch_limit = os.get_terminal_size()[0]
        os.system("clear")
        print(center_text(ch_limit, "RANChess"))
        print("")
        print_new_graphics(gen_graphics(chess_board))
        
        if message != "":
            if king_turn != 'K' and king_turn != 'k':
                
                if game_notation.get(plays) is None:
                    game_notation[plays] = [move]
                    print("")
                    print(center_text(ch_limit, "Last move --> " + "'" +
                                        move + "'" + " (White)"))
                    print(center_text(ch_limit, message))
               
                else:
                    game_notation[plays].append(move)
                    print("")
                    print(center_text(ch_limit, "Last move --> " + "'" +
                                        move + "'" + " (Black)"))
                    print(center_text(ch_limit, message))
             
            else:
                if message in [black_king_in_check, white_king_in_check]:
                    if king_turn == 'k':
                        game_notation[plays] = [move]
                        print("") 
                        print(center_text(ch_limit, "Last move --> " + "'" +
                                                move + "'" + " (White)"))
                        print(center_text(ch_limit, message))

                    elif king_turn == 'K':
                        game_notation[plays].append(move)
                        print("") 
                        print(center_text(ch_limit, "Last move --> " + "'" +
                                                move + "'" + " (Black)"))
                        print(center_text(ch_limit, message))
                        plays += 1

                else:
                    if game_notation.get(plays) is not None:
                        print("") 
                        wrong_move(move, message, king_turn)
                    
                    else:
                        print("")
                        wrong_move(move, message, king_turn)

        else:
            if king_turn == 'k':
                game_notation[plays] = [move]
                print("") 
                print(center_text(ch_limit, "Last move --> " + "'" +
                                        move + "'" + " (White)"))

            elif king_turn == 'K':
                game_notation[plays].append(move)
                print("") 
                print(center_text(ch_limit, "Last move --> " + "'" +
                                        move + "'" + " (Black)"))
                plays += 1
        print("")

if __name__ == '__main__':
    main()
