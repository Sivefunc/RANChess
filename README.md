# RANChess
![Gameplay](gameplay.png)

RANChess is a CLI program written in Python, that utilizes
RAN(Reversible Algebraic Notation) to make the given movement.

## Made by [Sivefunc](https://gitlab.com/sivefunc)
## Licensed under [GPLv3](LICENSE)
