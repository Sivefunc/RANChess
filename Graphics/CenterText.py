def center_text(ch_limit, text):
    center = int(ch_limit / 2 + 0.5)
    sep = center - int(len(text) / 2 + 0.5)
    if ch_limit == len(text):
        return text
    
    elif len(text) % 2 == 0:
        return sep * ' ' + text + (sep - 1) * ' '
    return sep * ' ' + text + sep * ' '

    
