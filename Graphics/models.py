white_king = "\u2654"
white_queen = "\u2655"
white_rook = "\u2656"
white_bishop = "\u2657"
white_knight = "\u2658"
white_pawn = "\u2659"

black_king = "\u265A"
black_queen = "\u265B"
black_rook = "\u265C"
black_bishop = "\u265D"
black_knight = "\u265E"
black_pawn = "\u265F"

pieces = {
        'K': white_king,
        'Q': white_queen,
        'R': white_rook,
        'B': white_bishop,
        'N': white_knight,
        'P': white_pawn,
        'k': black_king,
        'q': black_queen,
        'r': black_rook,
        'b': black_bishop,
        'n': black_knight,
        'p': black_pawn
        }

white_square = "| x "
black_square = "|:x:"
